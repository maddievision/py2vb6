import ast
import StringIO

def qp(s):
    return ast.dump(ast.parse(s,"<file>","single"))

def py2vb6(ins):
    vsa = VB6Visitor()
    vsa.visit(ast.parse(ins))
    print vsa.output()



class VB6Visitor(ast.NodeVisitor):
    def __init__(self):
        self.out = StringIO.StringIO()
        self.ctxs = []
        self.scopes = []
        self.nodetype = ""
        ast.NodeVisitor.__init__(self)
        self.counter = 0
    def newid(self):
        self.counter += 1
        return self.counter
    def raisex(self,node,s):
        raise Exception("Scope: %s, Context: %s: (%s) %s" % (self.scopelist(),self.ctx(),type(node).__name__,s))
    def pushscope(self,s):
        self.scopes.append({"__scope__":s})
    def popscope(self):
        self.scopes.pop()
    def scope(self):
        return self.scopes[-1]
    def scopelist(self):
        return [s["__scope__"] for s in self.scopes]
    def scopename(self):
        return self.scopes[-1]["__scope__"]
    def scopeaddname(self,name,ctx):
        if name not in self.scope():
            self.scope()[name] = {'types': []}
        if ctx not in self.scope()[name]['types']: self.scope()[name]['types'].append(ctx)
    def pushctx(self,s):
        self.ctxs.append(s)
    def popctx(self):
        self.ctxs.pop()
    def swapctx(self,s):
        self.popctx()
        self.pushctx(s)
    def ctx(self):
        return self.ctxs[-1]
    def write(self,text="",newln=True):
        if newln:
            self.out.write("%s\n" % text)
        else:
            self.out.write("%s" % text)
    #root
    def visit_Interactive(self,node):
        out = ""
        pre = ""
        self.pushscope("root")
        self.pushctx("body")
        for child in node.body:
            out += self.visit(child)
        for k in self.scope().keys():
            if k != "__scope__":
                v = self.scope()[k]
                vt = "Variant"
                if len(v['types']) == 1:
                    vt = v['types'][0]
                pre += "Dim %s as %s" % (k,vt)
                pre += "\n"
        self.popscope()
        self.write(pre+out,False)
    def visit_Module(self,node):
        self.visit_Interactive(node)
    #expressions
    def visit_Expr(self,node):
        if self.ctx() in ["body"]:
            self.raisex(node,"Invalid in context")
        return ""
    #print
    def visit_Print(self,node):
        out = "Debug.Print "
        self.pushctx("values")
        for child in node.values:
            out += self.visit(child)
        self.popctx()
        out += "\n"
        return out
    def value_for_assignment(self,node):
        out = ""
        pre = ""
        typ = "Variant"
        isset = False
        if type(node) == ast.List or type(node) == ast.Tuple:
            p = ""
            x = []
            for j,child in enumerate(node.elts):
                nx,np,nt,ns = self.value_for_assignment(child)
                p += np
                x.append(nx)
            objid = self.newid()
            self.scopeaddname("temp_list%d" % objid, "Collection")
            pre += p
            pre += "Set temp_list%d = new Collection()" % objid            
            pre += "\n"
            for a in x:
                pre += "temp_list%d.Add %s" % (objid, a)
                pre += "\n"
            out = "temp_list%d" % objid
            typ = "Collection"
            isset = True
        elif type(node) == ast.Dict:
            p = ""
            x = []
            for j,key,child in zip(xrange(len(node.keys)),node.keys,node.values):
                nx,np,nt,ns = self.value_for_assignment(child)
                p += np
                k = self.visit(key)
                x.append(tuple([ns,k,nx]))
            objid = self.newid()
            self.scopeaddname("temp_dict%d" % objid, "Object")
            pre += p
            pre += "Set temp_dict%d = CreateObject(\"Scripting.Dictionary\")" % objid            
            pre += "\n"
            for s,k,a in x:
                if s: pre += "Set "
                pre += "temp_dict%d(%s) = %s" % (objid, k, a)
                pre += "\n"
            out = "temp_dict%d" % objid
            typ = "Object"
            isset = True
        else:
            pre = ""
            out = self.visit(node)
            typ = "Variant"
        return out,pre,typ,isset
    def visit_Assign(self,node):
        out = ""
        val,pre,typ,isset = self.value_for_assignment(node.value)
        out += pre
        for i,target in enumerate(node.targets):
            self.pushctx("name")
            #process value
            if type(target) == ast.Tuple:
                objid = self.newid()
                self.scopeaddname("temp_obj%d" % objid, "Object")
                out += "Set temp_obj%d = " % objid
                self.pushctx("assign")
                out += val
                out += "\n"
                self.swapctx("name")
                for j,child in enumerate(target.elts):
                    out += self.visit(child)
                    out += " = temp_obj%d(%d)" % (objid,j+1)
                    out += "\n"
            else:
                if isset: out += "Set "
                out += self.visit(target)
                out += " = "
                self.swapctx("assign")
                out += val
                out += "\n"
                self.popctx
        self.popctx
        return out
    #BinOps
    def visit_Subscript(self,node):
        out = ""
        out += self.visit(node.value)
        if type(node.slice) == ast.Index:
            out += "("
            nout,pre,typ,isset = self.value_for_assignment(node.slice.value)
            out += nout
            out += ")"
        return pre+out
    def visit_Name(self,node):
        if type(node.ctx) == ast.Store: self.scopeaddname(node.id, "Variant")
        return node.id
    def visit_BinOp(self,node):
        self.pushctx("values")
        out = "("
        out += self.visit(node.left)
        self.swapctx("op")
        out += self.visit(node.op)
        self.swapctx("values")
        out += self.visit(node.right)
        out += ")"
        self.popctx()
        return out
    def visit_Add(self,node):
        return " + "
    def visit_Sub(self,node):
        return " - "
    def visit_FloorDiv(self,node):
        return " \\ "
    def visit_Mult(self,node):
        return " * "
    def visit_Pow(self,node):
        return " ^ "
    def visit_Div(self,node):
        return " / "
    #Literals
    def visit_Num(self,node):
        return "%d" % node.n
    def visit_Str(self,node):
        return '"%s"' % node.s.replace('"','""')
    def output(self):
        self.out.flush()
        return self.out.getvalue()


print py2vb6("print 'this is a test'")
